import com.fanproject.Fan;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FanTest {
    @Test
    void defaultSettingsTest(){
        Fan fan = new Fan();
        Assertions.assertEquals(Fan.Speed.Off, fan.getSpeed());
        Assertions.assertEquals(Fan.Direction.Clockwise, fan.getDirection());
    }

    @Test
    void presetSettingsTest(){
        Fan fan = new Fan(Fan.Speed.Fast, Fan.Direction.Counterclockwise);
        Assertions.assertEquals(Fan.Speed.Fast, fan.getSpeed());
        Assertions.assertEquals(Fan.Direction.Counterclockwise, fan.getDirection());
    }

    @Test
    void speedPullTest(){
        Fan fan = new Fan();
        fan.speedCordPulled();
        Assertions.assertEquals(Fan.Speed.Slow, fan.getSpeed());
    }

    @Test
    void directionPullTest(){
        Fan fan = new Fan();
        fan.reverseCordPulled();
        Assertions.assertEquals(Fan.Direction.Counterclockwise, fan.getDirection());
    }

    @Test
    void speedLoopTest(){
        Fan fan = new Fan(Fan.Speed.Fast, Fan.Direction.Clockwise);
        fan.speedCordPulled();
        Assertions.assertEquals(Fan.Speed.Off, fan.getSpeed());
    }

    @Test
    void directionLoopTest(){
        Fan fan = new Fan(Fan.Speed.Off, Fan.Direction.Counterclockwise);
        fan.reverseCordPulled();
        Assertions.assertEquals(Fan.Direction.Clockwise, fan.getDirection());
    }
}
