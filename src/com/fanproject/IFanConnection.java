package com.fanproject;

public interface IFanConnection {
    void connect(String connectioninfo);
    Fan.Speed getSpeedStatus();
    Fan.Direction getDirectionStatus();
    void sendCommand(byte[] commandCode);
}
