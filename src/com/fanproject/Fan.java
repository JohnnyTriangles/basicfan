package com.fanproject;

public class Fan {

    private Speed _speed;
    private Direction _direction;

    public Fan(){
        _speed = Speed.Off;
        _direction = Direction.Clockwise;
    }

    public Fan(Speed s, Direction d){
        _speed = s;
        _direction = d;
    }

    public Speed getSpeed(){
        return _speed;
    }

    public Direction getDirection(){
        return _direction;
    }

    public void speedCordPulled(){
        _speed = _speed.next();
    }

    public void reverseCordPulled(){
        _direction = _direction.next();
    }

    public enum Speed{
        Off,
        Slow,
        Medium,
        Fast;

        public Speed next() {
            if(ordinal() == values().length-1)
                return values()[0];
            return values()[ordinal() + 1];
        }
    }

    public enum Direction{
        Clockwise,
        Counterclockwise;

        public Direction next() {
            if(ordinal() == values().length-1)
                return values()[0];
            return values()[ordinal() + 1];
        }
    }
}
